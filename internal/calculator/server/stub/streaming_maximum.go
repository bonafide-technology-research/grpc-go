package stub

import (
	"io"

	"google.golang.org/grpc"

	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// streamingMaximumServerStub is a receiver which encapsulates stubbed results for the StreamingMaximumServer contract.
type streamingMaximumServerStub struct {
	resultCount int
	RecvError   error
	RecvResult  *protobuf.StreamingMaximumRequest
	SendError   error
	// Embed default grpc.ServerStream to facilitate implementing protobuf.CalculatorService_StreamingSumServer.
	grpc.ServerStream
}

// NewStreamingMaximumServerStub is a factory function called to return an initialized instance.
func NewStreamingMaximumServerStub(
	rRes *protobuf.StreamingMaximumRequest,
	sErr, rErr error) *streamingMaximumServerStub {
	return &streamingMaximumServerStub{
		RecvError:  rErr,
		RecvResult: rRes,
		SendError:  sErr,
	}
}

// Send is called to return a stubbed result.
func (s *streamingMaximumServerStub) Send(_ *protobuf.StreamingMaximumResponse) error {
	return s.SendError
}

// Recv is called to return a stubbed result.
func (s *streamingMaximumServerStub) Recv() (*protobuf.StreamingMaximumRequest, error) {
	if s.RecvError == nil {
		if s.resultCount == 1 {
			return nil, io.EOF
		} else {
			s.resultCount += 1
			return s.RecvResult, nil
		}
	}
	return nil, s.RecvError
}
