package stub

import (
	"errors"
	"io"
	"math/rand"
	"testing"

	"github.com/brianvoe/gofakeit/v5"
	"github.com/stretchr/testify/assert"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/request"
	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/response"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// TestNewStreamingAverageServerStub tests streamingAverageServerStub.NewStreamingAverageServerStub.
func TestNewStreamingAverageServerStub(t *testing.T) {
	res := NewStreamingAverageServerStub(nil, nil, nil)

	assert.Equal(t, &streamingAverageServerStub{}, res)
}

// TestStreamingAverageServerStub_Recv tests streamingAverageServerStub.Recv.
func TestStreamingAverageServerStub_Recv(t *testing.T) {
	type testCase struct {
		name           string
		s              *streamingAverageServerStub
		expectedResult *protobuf.StreamingAverageRequest
		expectedError  error
	}

	testCases := []testCase{
		func() testCase {
			r := request.NewStreamingAverageRequest(gofakeit.Name(), rand.Int63()).Request
			return testCase{
				name:           "non-error",
				s:              NewStreamingAverageServerStub(nil, r, nil),
				expectedResult: r,
				expectedError:  nil,
			}
		}(),
		func() testCase {
			r := request.NewStreamingAverageRequest(gofakeit.Name(), rand.Int63()).Request
			s := NewStreamingAverageServerStub(nil, r, nil)
			_, e := s.Recv()
			if e != nil {
				t.Fatalf(e.Error())
			}
			return testCase{
				name:           "EOF",
				s:              s,
				expectedResult: nil,
				expectedError:  io.EOF,
			}
		}(),
		func() testCase {
			e := errors.New(gofakeit.Sentence(100))
			return testCase{
				name:           "error",
				s:              NewStreamingAverageServerStub(nil, nil, e),
				expectedResult: nil,
				expectedError:  e,
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := testCases[i].s

			res, e := sut.Recv()

			assert.Equal(t, testCases[i].expectedResult, res)
			assert.Equal(t, testCases[i].expectedError, e)
		})
	}
}

// TestStreamingAverageServerStub_SendAndClose tests streamingAverageServerStub.SendAndClose.
func TestStreamingAverageServerStub_SendAndClose(t *testing.T) {
	type testCase struct {
		name          string
		expectedError error
	}

	testCases := []testCase{
		func() testCase {
			e := errors.New(gofakeit.Sentence(100))
			return testCase{
				name:          "error",
				expectedError: e,
			}
		}(),
		func() testCase {
			return testCase{
				name:          "non-error",
				expectedError: nil,
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := NewStreamingAverageServerStub(testCases[i].expectedError, nil, nil)

			e := sut.SendAndClose(response.NewStreamingAverageResponse(rand.Float64()))

			assert.Equal(t, testCases[i].expectedError, e)
		})
	}
}
