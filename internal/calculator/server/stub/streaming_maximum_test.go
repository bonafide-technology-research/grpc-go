package stub

import (
	"errors"
	"io"
	"math/rand"
	"testing"

	"github.com/brianvoe/gofakeit/v5"
	"github.com/stretchr/testify/assert"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/request"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// TestNewStreamingMaximumServerStub tests stub.NewStreamingMaximumServerStub.
func TestNewStreamingMaximumServerStub(t *testing.T) {
	res := NewStreamingMaximumServerStub(nil, nil, nil)

	assert.IsType(t, &streamingMaximumServerStub{}, res)
}

// TestStreamingMaximumServerStub_Recv tests streamingMaximumServerStub.Recv.
func TestStreamingMaximumServerStub_Recv(t *testing.T) {
	type testCase struct {
		name           string
		r              *streamingMaximumServerStub
		expectedResult *protobuf.StreamingMaximumRequest
		expectedError  error
	}

	testCases := []testCase{
		func() testCase {
			e := errors.New(gofakeit.Sentence(100))
			return testCase{
				name:           "error",
				r:              NewStreamingMaximumServerStub(nil, nil, e),
				expectedResult: nil,
				expectedError:  e,
			}
		}(),
		func() testCase {
			r := request.NewStreamingMaxRequest(rand.Int63()).Request
			return testCase{
				name:           "new number",
				r:              NewStreamingMaximumServerStub(r, nil, nil),
				expectedResult: r,
				expectedError:  nil,
			}
		}(),
		func() testCase {
			r := NewStreamingMaximumServerStub(request.NewStreamingMaxRequest(rand.Int63()).Request, nil, nil)
			_, err := r.Recv()
			if err != nil {
				t.Fatalf(err.Error())
			}
			return testCase{
				name:           "EOF",
				r:              r,
				expectedResult: nil,
				expectedError:  io.EOF,
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := testCases[i].r

			res, err := sut.Recv()

			assert.Equal(t, testCases[i].expectedResult, res)
			assert.Equal(t, testCases[i].expectedError, err)
		})
	}
}

// TestStreamingMaximumServerStub_Send tests streamingMaximumServerStub.Send.
func TestStreamingMaximumServerStub_Send(t *testing.T) {
	type testCase struct {
		name          string
		expectedError error
	}

	testCases := []testCase{
		{
			name:          "error",
			expectedError: errors.New(gofakeit.Sentence(100)),
		},
		{
			name:          "non-error",
			expectedError: nil,
		},
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := NewStreamingMaximumServerStub(nil, testCases[i].expectedError, nil)

			e := sut.Send(nil)

			assert.Equal(t, testCases[i].expectedError, e)
		})
	}
}
