package stub

import (
	"google.golang.org/grpc"

	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// serverStub is a stubbed implementation of protobuf.CalculatorService_StreamingSumServer which enables unit-testing.
type serverStub struct {
	SendError error
	// Embed default grpc.ServerStream to facilitate implementing protobuf.CalculatorService_StreamingSumServer.
	grpc.ServerStream
}

// NewServerStub is a factory function called to return an initialized instance.
func NewServerStub(sendError error) *serverStub {
	return &serverStub{
		SendError: sendError,
	}
}

// Send implements protobuf.CalculatorService_StreamingSumServer. Send is called to return stubbed error.
func (s *serverStub) Send(_ *protobuf.StreamingSumResponse) error {
	return s.SendError
}
