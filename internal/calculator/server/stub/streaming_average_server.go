package stub

import (
	"io"

	"google.golang.org/grpc"

	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// streamingAverageServerStub is a receiver which encapsulates stubbed results for the StreamingAverageServer contract.
type streamingAverageServerStub struct {
	resultCount       int
	SendAndCloseError error
	RecvResult        *protobuf.StreamingAverageRequest
	RecvError         error
	// Embed default grpc.ServerStream to facilitate implementing protobuf.CalculatorService_StreamingSumServer.
	grpc.ServerStream
}

// NewStreamingAverageServerStub is a factory function called to return an initialized instance.
func NewStreamingAverageServerStub(
	sErr error,
	rRes *protobuf.StreamingAverageRequest,
	rErr error) *streamingAverageServerStub {
	return &streamingAverageServerStub{
		SendAndCloseError: sErr,
		RecvResult:        rRes,
		RecvError:         rErr,
	}
}

// SendAndClose is called to return a stubbed result.
func (s *streamingAverageServerStub) SendAndClose(_ *protobuf.StreamingAverageResponse) error {
	return s.SendAndCloseError
}

// RecV is called to return a stubbed result.
func (s *streamingAverageServerStub) Recv() (*protobuf.StreamingAverageRequest, error) {
	if s.RecvError == nil {
		if s.resultCount == 1 {
			return nil, io.EOF
		} else {
			s.resultCount += 1
			return s.RecvResult, nil
		}
	}
	return nil, s.RecvError
}
