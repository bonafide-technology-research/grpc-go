package stub

import (
	"errors"
	"testing"

	"github.com/brianvoe/gofakeit/v5"
	"github.com/stretchr/testify/assert"

	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// TestNewServerStub tests server.NewServerStub.
func TestNewServerStub(t *testing.T) {
	res := NewServerStub(nil)

	assert.IsType(t, &serverStub{}, res)
}

// TestServerStub_Send tests serverStub.Send.
func TestServerStub_Send(t *testing.T) {
	type testCase struct {
		name          string
		r             *serverStub
		expectedError error
	}

	testCases := []testCase{
		func() testCase {
			e := errors.New(gofakeit.Sentence(100))
			return testCase{
				name:          "send with error",
				r:             NewServerStub(e),
				expectedError: e,
			}
		}(),
		func() testCase {
			return testCase{
				name:          "send without error",
				r:             NewServerStub(nil),
				expectedError: nil,
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := testCases[i].r

			e := sut.Send(&protobuf.StreamingSumResponse{})

			assert.Equal(t, testCases[i].expectedError, e)
		})
	}
}
