package store

// AverageStore is a contract responsible for managing averages for the gRPC StreamingAverage procedure.
type AverageStore interface {
	// Put is called to store a number for the given client ID.
	Put(clientId string, number int64)

	// Get is called to retrieve the stored number for the given ID.
	Get(clientId string) ([]int64, error)
}
