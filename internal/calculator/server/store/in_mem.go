package store

import (
	"errors"
	"fmt"
)

// InMemAverageStore is a receiver that encapsulates required dependencies required for implementing
// store.AverageStore.
type InMemAverageStore struct {
	numbersMap map[string][]int64
}

// NewInMemAverageStore is a factory function called to return an initialized instance.
func NewInMemAverageStore() *InMemAverageStore {
	return &InMemAverageStore{numbersMap: make(map[string][]int64)}
}

// handleMissingKeyError is called to return a well-defined error when a non-existing client ID is given.
func (i *InMemAverageStore) handleMissingKeyError(clientId string) error {
	return errors.New(fmt.Sprintf("InMemAverageStore: clientId '%s' does not exist", clientId))
}

// Get is called to return the stored number for the given client ID. If the client ID key does not exist, and error
// is thrown.
func (i *InMemAverageStore) Get(clientId string) ([]int64, error) {
	if i.numbersMap[clientId] == nil {
		return nil, i.handleMissingKeyError(clientId)
	}
	return i.numbersMap[clientId], nil
}

// Put is called to store the specified number for the given client ID.
func (i *InMemAverageStore) Put(clientId string, number int64) {
	i.numbersMap[clientId] = append(i.numbersMap[clientId], number)
}
