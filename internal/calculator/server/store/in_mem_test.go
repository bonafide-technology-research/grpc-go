package store

import (
	"math/rand"
	"testing"

	"github.com/brianvoe/gofakeit/v5"
	"github.com/stretchr/testify/assert"
)

// TestNewInMemAverageStore tests store.NewInMemAverageStore.
func TestNewInMemAverageStore(t *testing.T) {
	res := NewInMemAverageStore()

	assert.IsType(t, &InMemAverageStore{}, res)
}

// TestInMemAverageStore_Get tests InMemAverageStore.Get.
func TestInMemAverageStore_Get(t *testing.T) {
	type testCase struct {
		name           string
		clientId       string
		numbersMap     map[string][]int64
		expectedResult []int64
		expectedError  func(sut *InMemAverageStore) error
	}

	testCases := []testCase{
		func() testCase {
			c := gofakeit.Name()
			n := make(map[string][]int64)
			n[c] = []int64{rand.Int63()}
			return testCase{
				name:           "existing number",
				clientId:       c,
				numbersMap:     n,
				expectedResult: n[c],
				expectedError: func(sut *InMemAverageStore) error {
					return nil
				},
			}
		}(),
		func() testCase {
			c := gofakeit.Name()
			return testCase{
				name:           "non-existing number",
				clientId:       c,
				numbersMap:     make(map[string][]int64),
				expectedResult: nil,
				expectedError: func(sut *InMemAverageStore) error {
					return sut.handleMissingKeyError(c)
				},
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := NewInMemAverageStore()
			sut.numbersMap = testCases[i].numbersMap

			res, err := sut.Get(testCases[i].clientId)

			assert.Equal(t, testCases[i].expectedResult, res)
			assert.Equal(t, testCases[i].expectedError(sut), err)
		})
	}
}

// TestInMemAverageStore_Get tests InMemAverageStore.Put.
func TestInMemAverageStore_Put(t *testing.T) {
	type testCase struct {
		name           string
		clientId       string
		numbers        []int64
		expectedResult []int64
	}

	testCases := []testCase{
		func() testCase {
			n := rand.Int63()
			return testCase{
				name:           "put number",
				clientId:       gofakeit.Name(),
				numbers:        []int64{n},
				expectedResult: []int64{n},
			}
		}(),
		func() testCase {
			n := rand.Int63()
			n2 := rand.Int63()
			return testCase{
				name:           "put second number",
				clientId:       gofakeit.Name(),
				numbers:        []int64{n, n2},
				expectedResult: []int64{n, n2},
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := NewInMemAverageStore()

			for j := range testCases[i].numbers {
				sut.Put(testCases[i].clientId, testCases[i].numbers[j])
			}

			assert.Equal(t, testCases[i].expectedResult, sut.numbersMap[testCases[i].clientId])
		})
	}
}
