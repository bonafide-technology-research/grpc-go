package server

import (
	"context"
	"errors"
	"math/rand"
	"testing"

	"github.com/brianvoe/gofakeit/v5"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/err"
	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/request"
	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/response"
	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/server/store"
	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/server/stub"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// TestNew tests server.New.
func TestNew(t *testing.T) {
	res := New(store.NewInMemAverageStore())

	assert.IsType(t, &gRPCServer{}, res)
}

// TestServer_Sum tests server.Sum.
func TestServer_Sum(t *testing.T) {
	type testCase struct {
		name           string
		r              *protobuf.SumRequest
		expectedResult *protobuf.SumResponse
		expectedError  error
	}

	testCases := []testCase{
		func() testCase {
			r := request.NewSumRequest(-1, rand.Int63())
			return testCase{
				name:           "validation error",
				r:              r.Request,
				expectedResult: nil,
				expectedError: status.Error(
					codes.InvalidArgument,
					err.NewInvalidRequest("SumRequest 'NumberOne' must be a positive number").Error(),
				),
			}
		}(),
		func() testCase {
			num1 := rand.Int63()
			num2 := rand.Int63()
			return testCase{
				name:           "return sum",
				r:              request.NewSumRequest(num1, num2).Request,
				expectedResult: response.NewSumResponse(num1 + num2),
				expectedError:  nil,
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := New(store.NewInMemAverageStore())

			res, e := sut.Sum(context.Background(), testCases[i].r)

			assert.Equal(t, testCases[i].expectedResult, res)
			assert.Equal(t, testCases[i].expectedError, e)
		})
	}
}

// TestGRPCServer_StreamingSum tests server.StreamingSum.
func TestGRPCServer_StreamingSum(t *testing.T) {
	type testCase struct {
		name          string
		r             *protobuf.StreamingSumRequest
		s             protobuf.CalculatorService_StreamingSumServer
		expectedError error
	}

	testCases := []testCase{
		func() testCase {
			return testCase{
				name: "invalid StreamingSumRequest",
				r:    &protobuf.StreamingSumRequest{Number: -1},
				s:    stub.NewServerStub(nil),
				expectedError: status.Error(
					codes.InvalidArgument,
					err.NewInvalidRequest("StreamingSumRequest 'number' cannot be < 0").Error(),
				),
			}
		}(),
		func() testCase {
			e := errors.New(gofakeit.Sentence(100))
			return testCase{
				name:          "send error",
				r:             &protobuf.StreamingSumRequest{Number: rand.Int63()},
				s:             stub.NewServerStub(e),
				expectedError: status.Error(codes.Internal, e.Error()),
			}
		}(),
		func() testCase {
			return testCase{
				name:          "send",
				r:             &protobuf.StreamingSumRequest{Number: rand.Int63()},
				s:             stub.NewServerStub(nil),
				expectedError: nil,
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := New(store.NewInMemAverageStore())

			e := sut.StreamingSum(testCases[i].r, testCases[i].s)

			assert.Equal(t, testCases[i].expectedError, e)
		})
	}
}

// TestGRPCServer_StreamingAverage tests server.StreamingAverage.
func TestGRPCServer_StreamingAverage(t *testing.T) {
	type testCase struct {
		name          string
		stream        protobuf.CalculatorService_StreamingAverageServer
		expectedError error
	}

	testCases := []testCase{
		func() testCase {
			r := request.NewStreamingAverageRequest(gofakeit.Name(), rand.Int63())
			return testCase{
				name:          "new number",
				stream:        stub.NewStreamingAverageServerStub(nil, r.Request, nil),
				expectedError: nil,
			}
		}(),
		func() testCase {
			e := errors.New(gofakeit.Sentence(10))
			return testCase{
				name:          "recv error",
				stream:        stub.NewStreamingAverageServerStub(nil, nil, e),
				expectedError: status.Error(codes.Internal, e.Error()),
			}
		}(),
		func() testCase {
			e := errors.New(gofakeit.Sentence(10))
			return testCase{
				name:          "recv error",
				stream:        stub.NewStreamingAverageServerStub(nil, nil, e),
				expectedError: status.Error(codes.Internal, e.Error()),
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := New(store.NewInMemAverageStore())

			e := sut.StreamingAverage(testCases[i].stream)

			assert.Equal(t, testCases[i].expectedError, e)
		})
	}
}

// TestGRPCServer_StreamingMaximum tests gRPC.StreamingMaximum.
func TestGRPCServer_StreamingMaximum(t *testing.T) {
	type testCase struct {
		name          string
		stream        protobuf.CalculatorService_StreamingMaximumServer
		expectedError error
	}

	testCases := []testCase{
		func() testCase {
			n := rand.Int63()
			s := stub.NewStreamingMaximumServerStub(request.NewStreamingMaxRequest(n).Request, nil, nil)
			e := s.Send(response.NewStreamingMaximumResponse(n))
			if e != nil {
				t.Fatalf(e.Error())
			}
			return testCase{
				name:          "EOF",
				stream:        s,
				expectedError: nil,
			}
		}(),
		func() testCase {
			e := errors.New(gofakeit.Sentence(100))
			return testCase{
				name:          "receive error",
				stream:        stub.NewStreamingMaximumServerStub(nil, nil, e),
				expectedError: status.Error(codes.Internal, e.Error()),
			}
		}(),
		func() testCase {
			e := errors.New(gofakeit.Sentence(100))
			r := request.NewStreamingMaxRequest(rand.Int63()).Request
			return testCase{
				name:          "send error",
				stream:        stub.NewStreamingMaximumServerStub(r, e, nil),
				expectedError: status.Error(codes.Internal, e.Error()),
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := New(store.NewInMemAverageStore())

			e := sut.StreamingMaximum(testCases[i].stream)

			assert.Equal(t, testCases[i].expectedError, e)
		})
	}
}
