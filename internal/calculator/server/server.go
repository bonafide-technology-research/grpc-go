package server

import (
	"context"
	"fmt"
	"io"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/request"
	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/response"
	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/server/store"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// gRPCServer is a receiver that encapsulates required dependencies required for a gRPC Calculator Service.
type gRPCServer struct {
	serverStreamingSum int64
	averageStore       store.AverageStore
	// Embedding the unimplemented server.
	protobuf.UnimplementedCalculatorServiceServer
}

// New is a factory function that returns an initialized server.gRPCServer instance.
func New(averageStore store.AverageStore) *gRPCServer {
	return &gRPCServer{
		averageStore: averageStore,
	}
}

// Sum is called to return the sum of number provided in the protobuf.SumRequest as a protobuf.SumResponse.
func (s *gRPCServer) Sum(_ context.Context, req *protobuf.SumRequest) (*protobuf.SumResponse, error) {

	d := request.NewSumRequest(req.NumberOne, req.NumberTwo)
	_, err := d.Validate()
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return response.NewSumResponse(d.Request.NumberOne + d.Request.NumberTwo), nil
}

// StreamingSum is called to return the new streamingSum total. If a validation error occurs, an error will be returned
// at the gRPC level. If a send error occurs, the error will be treated as an internal error.
func (s *gRPCServer) StreamingSum(req *protobuf.StreamingSumRequest, stream protobuf.CalculatorService_StreamingSumServer) error {
	fmt.Printf("Received number: %d\n", req.Number)
	d := request.NewStreamingSumRequest(req.Number)
	_, err := d.Validate()
	if err != nil {
		return status.Error(codes.InvalidArgument, err.Error())
	}

	s.serverStreamingSum = s.serverStreamingSum + req.Number
	if err := stream.Send(response.NewStreamingSumResponse(s.serverStreamingSum)); err != nil {
		return status.Error(codes.Internal, err.Error())
	}

	return nil
}

// StreamingAverage is called to receive a stream of numbers and average them. If a validation error occurs, an error
// will be returned at the gRPC level. If a send error occurs, the error will be treated as an internal error.
func (s *gRPCServer) StreamingAverage(stream protobuf.CalculatorService_StreamingAverageServer) error {
	var currentClientId string
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			n, _ := s.averageStore.Get(currentClientId)
			sum := int64(0)
			for i := range n {
				sum += n[i]
			}

			res := float64(sum) / float64(len(n))
			fmt.Printf("Total: %d Count: %d Avg: %f", sum, len(n), res)
			return stream.SendAndClose(response.NewStreamingAverageResponse(res))
		}
		if err != nil {
			return status.Error(codes.Internal, err.Error())
		}

		fmt.Println("Received request from client: ", req.GetClientId(), " - ", req.GetNumber())
		currentClientId = req.GetClientId()
		s.averageStore.Put(req.GetClientId(), req.GetNumber())
	}
}

func (s *gRPCServer) StreamingMaximum(stream protobuf.CalculatorService_StreamingMaximumServer) error {
	max := int64(0)
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			fmt.Println("ending bi-directional stream")
			return nil
		}
		if err != nil {
			return status.Error(codes.Internal, err.Error())
		}
		if req.GetNumber() > max {
			max = req.GetNumber()
			err := stream.Send(response.NewStreamingMaximumResponse(req.GetNumber()))
			if err != nil {
				return status.Error(codes.Internal, err.Error())
			}
		}
	}
}
