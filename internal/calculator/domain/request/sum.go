package request

import (
	"fmt"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/err"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// sumRequestDecorator decorates the gRPC generated protobuf.SumRequest.
type sumRequestDecorator struct {
	Request *protobuf.SumRequest
}

// NewSumRequest is factory function that is called to return an initialized instance of a *protobuf.SumRequest.
func NewSumRequest(x, y int64) *sumRequestDecorator {
	d := &sumRequestDecorator{}
	d.Request = &protobuf.SumRequest{
		NumberOne: x,
		NumberTwo: y,
	}

	return d
}

// handleEmptySumRequest is called to return a err.NewInvalidRequest error specific to a nil request; enables
// unit-testing.
func (*sumRequestDecorator) handleEmptySumRequest() error {
	return err.NewInvalidRequest("SumRequest cannot be nil")
}

// handleNegativeNumber is called to return a err.NewInvalidRequest error specific to an invalid number; enables
// unit-testing.
func (*sumRequestDecorator) handleNegativeNumber(fieldName string) error {
	return err.NewInvalidRequest(fmt.Sprint("SumRequest '", fieldName, "' must be a positive number"))
}

// Validate fulfills the Validator interface. Validate is called to return whether or not the request object is
// valid. If the request is not valid, an error is returned.
func (s *sumRequestDecorator) Validate() (bool, error) {
	if s.Request == nil {
		return false, s.handleEmptySumRequest()
	}

	if s.Request.NumberOne < 0 {
		return false, s.handleNegativeNumber("NumberOne")
	}

	if s.Request.NumberTwo < 0 {
		return false, s.handleNegativeNumber("NumberTwo")
	}

	return true, nil
}
