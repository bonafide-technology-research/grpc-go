package request

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestNewStreamingSumRequest tests request.NewStreamingSumRequest.
func TestNewStreamingSumRequest(t *testing.T) {
	num := rand.Int63()
	res := NewStreamingSumRequest(num)

	assert.Equal(t, NewStreamingSumRequest(num), res)
}

// TestStreamingSumRequest_Validate tests streamingSumRequestDecorator.Validate.
func TestStreamingSumRequest_Validate(t *testing.T) {
	type testCase struct {
		name           string
		r              *streamingSumRequestDecorator
		expectedResult bool
		expectedError  func(sut *streamingSumRequestDecorator) error
	}

	testCases := []testCase{
		func() testCase {
			return testCase{
				name:           "valid request",
				r:              NewStreamingSumRequest(rand.Int63()),
				expectedResult: true,
				expectedError: func(sut *streamingSumRequestDecorator) error {
					return nil
				},
			}
		}(),
		func() testCase {
			num := rand.Int63()
			return testCase{
				name:           "invalid number",
				r:              NewStreamingSumRequest(num - (2 * num)),
				expectedResult: false,
				expectedError: func(sut *streamingSumRequestDecorator) error {
					return sut.handleInvalidNumber()
				},
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := testCases[i].r

			res, err := sut.Validate()

			assert.Equal(t, testCases[i].expectedResult, res)
			assert.Equal(t, testCases[i].expectedError(sut), err)
		})
	}
}
