package request

import (
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// streamingAverageDecorator decorates the gRPC generated protobuf.StreamingAverageRequest.
type streamingAverageDecorator struct {
	Request *protobuf.StreamingAverageRequest
}

// NewStreamingAverageRequest is a factory function called to return an initialized instance.
func NewStreamingAverageRequest(clientId string, number int64) *streamingAverageDecorator {
	return &streamingAverageDecorator{
		Request: &protobuf.StreamingAverageRequest{
			ClientId: clientId,
			Number:   number,
		},
	}
}

// Validate implements the Validator interface. Validate is called to return whether or no the request is valid.
// If the request is not valid, a well-define error is returned.
func (s *streamingAverageDecorator) Validate() (bool, error) {
	return true, nil
}
