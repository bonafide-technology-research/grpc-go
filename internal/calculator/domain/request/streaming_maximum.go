package request

import "gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"

// streamingMaxDecorator decorates the gRPC generated protobuf.StreamingMaximumRequest.
type streamingMaxDecorator struct {
	Request *protobuf.StreamingMaximumRequest
}

// NewStreamingMaxRequest is a factory function called to return an initialized instance.
func NewStreamingMaxRequest(number int64) *streamingMaxDecorator {
	return &streamingMaxDecorator{Request: &protobuf.StreamingMaximumRequest{Number: number}}
}

// Validate implements the Validator interface. Validate is called to return whether or no the request is valid.
// If the request is not valid, a well-define error is returned.
func (s *streamingMaxDecorator) Validate() (bool, error) {
	return true, nil
}
