package request

import (
	"math/rand"
	"testing"

	"github.com/brianvoe/gofakeit/v5"
	"github.com/stretchr/testify/assert"

	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// TestNewStreamingAverageRequest tests request.NewStreamingAverageRequest.
func TestNewStreamingAverageRequest(t *testing.T) {
	c := gofakeit.Name()
	n := rand.Int63()
	res := NewStreamingAverageRequest(c, n)

	assert.Equal(
		t,
		&streamingAverageDecorator{
			Request: &protobuf.StreamingAverageRequest{
				ClientId: c,
				Number:   n,
			},
		},
		res,
	)
}

// TestStreamingAverageDecorator_Validate tests streamingAverageDecorator.Validate.
func TestStreamingAverageDecorator_Validate(t *testing.T) {
	type testCase struct {
		name           string
		r              *streamingAverageDecorator
		expectedResult bool
		expectedError  func(sut *streamingAverageDecorator) error
	}

	testCases := []testCase{
		{
			name:           "valid",
			r:              NewStreamingAverageRequest(gofakeit.Name(), rand.Int63()),
			expectedResult: true,
			expectedError: func(sut *streamingAverageDecorator) error {
				return nil
			},
		},
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := testCases[i].r

			res, err := sut.Validate()

			assert.Equal(t, testCases[i].expectedResult, res)
			assert.Equal(t, testCases[i].expectedError(sut), err)
		})
	}
}
