package request

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestNewStreamingMaxRequest tests request.NewStreamingMaxRequest.
func TestNewStreamingMaxRequest(t *testing.T) {
	res := NewStreamingMaxRequest(rand.Int63())

	assert.IsType(t, &streamingMaxDecorator{}, res)
}

// TestStreamingMaxDecorator_Validate tests streamingMaxDecorator.Validate.
func TestStreamingMaxDecorator_Validate(t *testing.T) {
	type testCase struct {
		name           string
		r              *streamingMaxDecorator
		expectedResult bool
		expectedError  error
	}

	testCases := []testCase{
		func() testCase {
			return testCase{
				name:           "valid",
				r:              NewStreamingMaxRequest(rand.Int63()),
				expectedResult: true,
				expectedError:  nil,
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := testCases[i].r

			res, err := sut.Validate()

			assert.Equal(t, testCases[i].expectedResult, res)
			assert.Equal(t, testCases[i].expectedError, err)
		})
	}
}
