package request

import (
	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/err"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// streamingSumRequestDecorator decorates the gRPC generated protobuf.StreamingSumRequest.
type streamingSumRequestDecorator struct {
	Request *protobuf.StreamingSumRequest
}

// NewStreamingSumRequest is a factory function called to return an initialized instance.
func NewStreamingSumRequest(number int64) *streamingSumRequestDecorator {
	return &streamingSumRequestDecorator{
		Request: &protobuf.StreamingSumRequest{
			Number: number,
		},
	}
}

// handleInvalidNumber is called to return a err.NewInvalidRequest specific to a number which is invalid; enables
// unit-testing.
func (s *streamingSumRequestDecorator) handleInvalidNumber() error {
	return err.NewInvalidRequest("StreamingSumRequest 'number' cannot be < 0")
}

// Validate implements the Validator interface. Validate is called to return whether or no the request is valid.
// If the request is not valid, a well-define error is returned.
func (s *streamingSumRequestDecorator) Validate() (bool, error) {
	if s.Request.Number < 0 {
		return false, s.handleInvalidNumber()
	}

	return true, nil
}
