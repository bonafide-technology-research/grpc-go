package request

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestNewSumRequest test sum.New.
func TestNewSumRequest(t *testing.T) {
	num1 := rand.Int63()
	num2 := rand.Int63()
	res := NewSumRequest(num1, num2)

	assert.Equal(t, NewSumRequest(num1, num2), res)
}

// TestDecorator_Validate tests sum.Validate.
func TestDecorator_Validate(t *testing.T) {
	type testCase struct {
		name           string
		r              *sumRequestDecorator
		expectedResult bool
		expectedError  func(sut *sumRequestDecorator) error
	}

	testCases := []testCase{
		func() testCase {
			r := NewSumRequest(rand.Int63(), rand.Int63())
			r.Request = nil
			return testCase{
				name:           "nil decorator.SumRequest",
				r:              r,
				expectedResult: false,
				expectedError: func(sut *sumRequestDecorator) error {
					return sut.handleEmptySumRequest()
				},
			}
		}(),
		func() testCase {
			n := rand.Int63()
			r := NewSumRequest(n-n*2, rand.Int63())
			return testCase{
				name:           "negative decorator.NumberOne",
				r:              r,
				expectedResult: false,
				expectedError: func(sut *sumRequestDecorator) error {
					return sut.handleNegativeNumber("NumberOne")
				},
			}
		}(),
		func() testCase {
			n := rand.Int63()
			r := NewSumRequest(rand.Int63(), n-n*2)
			return testCase{
				name:           "negative decorator.NumberTwo",
				r:              r,
				expectedResult: false,
				expectedError: func(sut *sumRequestDecorator) error {
					return sut.handleNegativeNumber("NumberTwo")
				},
			}
		}(),
		func() testCase {
			return testCase{
				name:           "valid decorator",
				r:              NewSumRequest(rand.Int63(), rand.Int63()),
				expectedResult: true,
				expectedError: func(sut *sumRequestDecorator) error {
					return nil
				},
			}
		}(),
	}

	for i := range testCases {
		t.Run(testCases[i].name, func(t *testing.T) {
			sut := testCases[i].r

			res, err := sut.Validate()

			assert.Equal(t, testCases[i].expectedResult, res)
			assert.Equal(t, testCases[i].expectedError(sut), err)
		})
	}

}
