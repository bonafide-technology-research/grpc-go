package response

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

// TestNewSumResponse tests response.NewSumResponse.
func TestNewSumResponse(t *testing.T) {
	res := NewSumResponse(rand.Int63())

	assert.IsType(t, &protobuf.SumResponse{}, res)
}

// TestNewStreamingSumResponse tests response.NewStreamingSumResponse.
func TestNewStreamingSumResponse(t *testing.T) {
	res := NewStreamingSumResponse(rand.Int63())

	assert.IsType(t, &protobuf.StreamingSumResponse{}, res)
}

// TestNewStreamingAverageResponse tests response.NewStreamingAverageResponse.
func TestNewStreamingAverageResponse(t *testing.T) {
	res := NewStreamingAverageResponse(rand.Float64())

	assert.IsType(t, &protobuf.StreamingAverageResponse{}, res)
}

// TestNewStreamingMaximumResponse tests response.NewStreamingMaximumResponse.
func TestNewStreamingMaximumResponse(t *testing.T) {
	res := NewStreamingMaximumResponse(rand.Int63())

	assert.IsType(t, &protobuf.StreamingMaximumResponse{}, res)
}
