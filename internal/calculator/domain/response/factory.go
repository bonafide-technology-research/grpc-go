package response

import "gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"

// NewSumResponse is factory function that is called to return an initialized instance of a *protobuf.SumResponse.
func NewSumResponse(result int64) *protobuf.SumResponse {
	return &protobuf.SumResponse{Result: result}
}

// NewStreamingSumResponse is a factory function that is called to return an initialized instance of a
// *protobuf.StreamingSumResponse.
func NewStreamingSumResponse(result int64) *protobuf.StreamingSumResponse {
	return &protobuf.StreamingSumResponse{Result: result}
}

// NewStreamingAverageResponse is a factory function that is called to return an initialized instance of
// *protobuf.StreamingAverageResponse.
func NewStreamingAverageResponse(result float64) *protobuf.StreamingAverageResponse {
	return &protobuf.StreamingAverageResponse{Result: result}
}

// NewStreamingMaximumResponse is a factory function that is called to return an initialized instance of
// *protobuf.StreamingMaximumResponse.
func NewStreamingMaximumResponse(result int64) *protobuf.StreamingMaximumResponse {
	return &protobuf.StreamingMaximumResponse{Result: result}
}
