package err

import (
	"testing"

	"github.com/brianvoe/gofakeit/v5"
	"github.com/stretchr/testify/assert"
)

// TestNewInvalidRequest tests err.NewInvalidRequest.
func TestNewInvalidRequest(t *testing.T) {
	errMsg := gofakeit.Name()
	res := NewInvalidRequest(errMsg)

	assert.Equal(t, NewInvalidRequest(errMsg), res)
}

// TestInvalidRequest_Error tests invalidRequest.Error.
func TestInvalidRequest_Error(t *testing.T) {
	errMsg := gofakeit.Name()
	sut := NewInvalidRequest(errMsg)

	res := sut.Error()

	assert.Equal(t, errMsg, res)
}
