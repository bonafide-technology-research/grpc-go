package err

// invalidRequest is an error type specific to request object which are invalid.
type invalidRequest struct {
	errMsg string
}

// NewInvalidRequest is a factory function called to return an initialized instance.
func NewInvalidRequest(errMsg string) error {
	return &invalidRequest{
		errMsg: errMsg,
	}
}

// Error implements error interface. Error is called to return the struct's error message.
func (i *invalidRequest) Error() string {
	return i.errMsg
}
