// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.14.0
// source: vend/protobuf/calculator.proto

package protobuf

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

// SumRequest is the request data-transfer-object containing numbers to be added.
type SumRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// number_one is the first integer to be added.
	NumberOne int64 `protobuf:"varint,1,opt,name=number_one,json=numberOne,proto3" json:"number_one,omitempty"`
	// number_two is the second integer to be added.
	NumberTwo int64 `protobuf:"varint,2,opt,name=number_two,json=numberTwo,proto3" json:"number_two,omitempty"`
}

func (x *SumRequest) Reset() {
	*x = SumRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vend_protobuf_calculator_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SumRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SumRequest) ProtoMessage() {}

func (x *SumRequest) ProtoReflect() protoreflect.Message {
	mi := &file_vend_protobuf_calculator_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SumRequest.ProtoReflect.Descriptor instead.
func (*SumRequest) Descriptor() ([]byte, []int) {
	return file_vend_protobuf_calculator_proto_rawDescGZIP(), []int{0}
}

func (x *SumRequest) GetNumberOne() int64 {
	if x != nil {
		return x.NumberOne
	}
	return 0
}

func (x *SumRequest) GetNumberTwo() int64 {
	if x != nil {
		return x.NumberTwo
	}
	return 0
}

// SumResponse is the response data-transfer-object containing the summation.
type SumResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// result holds the summation value.
	Result int64 `protobuf:"varint,1,opt,name=result,proto3" json:"result,omitempty"`
}

func (x *SumResponse) Reset() {
	*x = SumResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vend_protobuf_calculator_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SumResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SumResponse) ProtoMessage() {}

func (x *SumResponse) ProtoReflect() protoreflect.Message {
	mi := &file_vend_protobuf_calculator_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SumResponse.ProtoReflect.Descriptor instead.
func (*SumResponse) Descriptor() ([]byte, []int) {
	return file_vend_protobuf_calculator_proto_rawDescGZIP(), []int{1}
}

func (x *SumResponse) GetResult() int64 {
	if x != nil {
		return x.Result
	}
	return 0
}

// StreamingSumRequest is the request data-transfer-object containing streaming numbers to be summed.
type StreamingSumRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// number to be added to the streaming total.
	Number int64 `protobuf:"varint,1,opt,name=number,proto3" json:"number,omitempty"`
}

func (x *StreamingSumRequest) Reset() {
	*x = StreamingSumRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vend_protobuf_calculator_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StreamingSumRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StreamingSumRequest) ProtoMessage() {}

func (x *StreamingSumRequest) ProtoReflect() protoreflect.Message {
	mi := &file_vend_protobuf_calculator_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StreamingSumRequest.ProtoReflect.Descriptor instead.
func (*StreamingSumRequest) Descriptor() ([]byte, []int) {
	return file_vend_protobuf_calculator_proto_rawDescGZIP(), []int{2}
}

func (x *StreamingSumRequest) GetNumber() int64 {
	if x != nil {
		return x.Number
	}
	return 0
}

// StreamingSumResponse is the response data-transfer-object containing streaming numbers to be summed.
type StreamingSumResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// result holds the summation  value.
	Result int64 `protobuf:"varint,1,opt,name=result,proto3" json:"result,omitempty"`
}

func (x *StreamingSumResponse) Reset() {
	*x = StreamingSumResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vend_protobuf_calculator_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StreamingSumResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StreamingSumResponse) ProtoMessage() {}

func (x *StreamingSumResponse) ProtoReflect() protoreflect.Message {
	mi := &file_vend_protobuf_calculator_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StreamingSumResponse.ProtoReflect.Descriptor instead.
func (*StreamingSumResponse) Descriptor() ([]byte, []int) {
	return file_vend_protobuf_calculator_proto_rawDescGZIP(), []int{3}
}

func (x *StreamingSumResponse) GetResult() int64 {
	if x != nil {
		return x.Result
	}
	return 0
}

// StreamingAverageRequest is the request data-transfer-object containing streaming numbers to be averaged.
type StreamingAverageRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// clientId is an identifier for the client.
	ClientId string `protobuf:"bytes,1,opt,name=clientId,proto3" json:"clientId,omitempty"`
	// number to be averaged.
	Number int64 `protobuf:"varint,2,opt,name=number,proto3" json:"number,omitempty"`
}

func (x *StreamingAverageRequest) Reset() {
	*x = StreamingAverageRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vend_protobuf_calculator_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StreamingAverageRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StreamingAverageRequest) ProtoMessage() {}

func (x *StreamingAverageRequest) ProtoReflect() protoreflect.Message {
	mi := &file_vend_protobuf_calculator_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StreamingAverageRequest.ProtoReflect.Descriptor instead.
func (*StreamingAverageRequest) Descriptor() ([]byte, []int) {
	return file_vend_protobuf_calculator_proto_rawDescGZIP(), []int{4}
}

func (x *StreamingAverageRequest) GetClientId() string {
	if x != nil {
		return x.ClientId
	}
	return ""
}

func (x *StreamingAverageRequest) GetNumber() int64 {
	if x != nil {
		return x.Number
	}
	return 0
}

// StreamingAverageResponse is the response data-transfer-object containing streaming average.
type StreamingAverageResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// result holds the average.
	Result float64 `protobuf:"fixed64,1,opt,name=result,proto3" json:"result,omitempty"`
}

func (x *StreamingAverageResponse) Reset() {
	*x = StreamingAverageResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vend_protobuf_calculator_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StreamingAverageResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StreamingAverageResponse) ProtoMessage() {}

func (x *StreamingAverageResponse) ProtoReflect() protoreflect.Message {
	mi := &file_vend_protobuf_calculator_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StreamingAverageResponse.ProtoReflect.Descriptor instead.
func (*StreamingAverageResponse) Descriptor() ([]byte, []int) {
	return file_vend_protobuf_calculator_proto_rawDescGZIP(), []int{5}
}

func (x *StreamingAverageResponse) GetResult() float64 {
	if x != nil {
		return x.Result
	}
	return 0
}

// StreamingMaximumRequest is the request data-transfer-object containing streaming numbers to be evaluated for the
// maximum.
type StreamingMaximumRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Number int64 `protobuf:"varint,1,opt,name=number,proto3" json:"number,omitempty"`
}

func (x *StreamingMaximumRequest) Reset() {
	*x = StreamingMaximumRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vend_protobuf_calculator_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StreamingMaximumRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StreamingMaximumRequest) ProtoMessage() {}

func (x *StreamingMaximumRequest) ProtoReflect() protoreflect.Message {
	mi := &file_vend_protobuf_calculator_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StreamingMaximumRequest.ProtoReflect.Descriptor instead.
func (*StreamingMaximumRequest) Descriptor() ([]byte, []int) {
	return file_vend_protobuf_calculator_proto_rawDescGZIP(), []int{6}
}

func (x *StreamingMaximumRequest) GetNumber() int64 {
	if x != nil {
		return x.Number
	}
	return 0
}

// StreamingMaximumResponse is the response data-transfer-object containing streaming maximum.
type StreamingMaximumResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Result int64 `protobuf:"varint,1,opt,name=result,proto3" json:"result,omitempty"`
}

func (x *StreamingMaximumResponse) Reset() {
	*x = StreamingMaximumResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_vend_protobuf_calculator_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *StreamingMaximumResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StreamingMaximumResponse) ProtoMessage() {}

func (x *StreamingMaximumResponse) ProtoReflect() protoreflect.Message {
	mi := &file_vend_protobuf_calculator_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StreamingMaximumResponse.ProtoReflect.Descriptor instead.
func (*StreamingMaximumResponse) Descriptor() ([]byte, []int) {
	return file_vend_protobuf_calculator_proto_rawDescGZIP(), []int{7}
}

func (x *StreamingMaximumResponse) GetResult() int64 {
	if x != nil {
		return x.Result
	}
	return 0
}

var File_vend_protobuf_calculator_proto protoreflect.FileDescriptor

var file_vend_protobuf_calculator_proto_rawDesc = []byte{
	0x0a, 0x1e, 0x76, 0x65, 0x6e, 0x64, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f,
	0x63, 0x61, 0x6c, 0x63, 0x75, 0x6c, 0x61, 0x74, 0x6f, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x08, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x22, 0x4a, 0x0a, 0x0a, 0x53, 0x75,
	0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x6e, 0x75, 0x6d, 0x62,
	0x65, 0x72, 0x5f, 0x6f, 0x6e, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x6e, 0x75,
	0x6d, 0x62, 0x65, 0x72, 0x4f, 0x6e, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x6e, 0x75, 0x6d, 0x62, 0x65,
	0x72, 0x5f, 0x74, 0x77, 0x6f, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x6e, 0x75, 0x6d,
	0x62, 0x65, 0x72, 0x54, 0x77, 0x6f, 0x22, 0x25, 0x0a, 0x0b, 0x53, 0x75, 0x6d, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x22, 0x2d, 0x0a,
	0x13, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x53, 0x75, 0x6d, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x22, 0x2e, 0x0a, 0x14,
	0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x53, 0x75, 0x6d, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x22, 0x4d, 0x0a, 0x17,
	0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x41, 0x76, 0x65, 0x72, 0x61, 0x67, 0x65,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1a, 0x0a, 0x08, 0x63, 0x6c, 0x69, 0x65, 0x6e,
	0x74, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x63, 0x6c, 0x69, 0x65, 0x6e,
	0x74, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x06, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x22, 0x32, 0x0a, 0x18, 0x53,
	0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x41, 0x76, 0x65, 0x72, 0x61, 0x67, 0x65, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c,
	0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x01, 0x52, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x22,
	0x31, 0x0a, 0x17, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x4d, 0x61, 0x78, 0x69,
	0x6d, 0x75, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6e, 0x75,
	0x6d, 0x62, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6e, 0x75, 0x6d, 0x62,
	0x65, 0x72, 0x22, 0x32, 0x0a, 0x18, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x4d,
	0x61, 0x78, 0x69, 0x6d, 0x75, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x16,
	0x0a, 0x06, 0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06,
	0x72, 0x65, 0x73, 0x75, 0x6c, 0x74, 0x32, 0xdc, 0x02, 0x0a, 0x11, 0x43, 0x61, 0x6c, 0x63, 0x75,
	0x6c, 0x61, 0x74, 0x6f, 0x72, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x34, 0x0a, 0x03,
	0x53, 0x75, 0x6d, 0x12, 0x14, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53,
	0x75, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x15, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x75, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x00, 0x12, 0x51, 0x0a, 0x0c, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x53,
	0x75, 0x6d, 0x12, 0x1d, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74,
	0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x53, 0x75, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x1e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72,
	0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x53, 0x75, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x22, 0x00, 0x30, 0x01, 0x12, 0x5d, 0x0a, 0x10, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69,
	0x6e, 0x67, 0x41, 0x76, 0x65, 0x72, 0x61, 0x67, 0x65, 0x12, 0x21, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x41, 0x76,
	0x65, 0x72, 0x61, 0x67, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e,
	0x67, 0x41, 0x76, 0x65, 0x72, 0x61, 0x67, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x00, 0x28, 0x01, 0x12, 0x5f, 0x0a, 0x10, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e,
	0x67, 0x4d, 0x61, 0x78, 0x69, 0x6d, 0x75, 0x6d, 0x12, 0x21, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67, 0x4d, 0x61, 0x78,
	0x69, 0x6d, 0x75, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x65, 0x61, 0x6d, 0x69, 0x6e, 0x67,
	0x4d, 0x61, 0x78, 0x69, 0x6d, 0x75, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22,
	0x00, 0x28, 0x01, 0x30, 0x01, 0x42, 0x0a, 0x5a, 0x08, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_vend_protobuf_calculator_proto_rawDescOnce sync.Once
	file_vend_protobuf_calculator_proto_rawDescData = file_vend_protobuf_calculator_proto_rawDesc
)

func file_vend_protobuf_calculator_proto_rawDescGZIP() []byte {
	file_vend_protobuf_calculator_proto_rawDescOnce.Do(func() {
		file_vend_protobuf_calculator_proto_rawDescData = protoimpl.X.CompressGZIP(file_vend_protobuf_calculator_proto_rawDescData)
	})
	return file_vend_protobuf_calculator_proto_rawDescData
}

var file_vend_protobuf_calculator_proto_msgTypes = make([]protoimpl.MessageInfo, 8)
var file_vend_protobuf_calculator_proto_goTypes = []interface{}{
	(*SumRequest)(nil),               // 0: protobuf.SumRequest
	(*SumResponse)(nil),              // 1: protobuf.SumResponse
	(*StreamingSumRequest)(nil),      // 2: protobuf.StreamingSumRequest
	(*StreamingSumResponse)(nil),     // 3: protobuf.StreamingSumResponse
	(*StreamingAverageRequest)(nil),  // 4: protobuf.StreamingAverageRequest
	(*StreamingAverageResponse)(nil), // 5: protobuf.StreamingAverageResponse
	(*StreamingMaximumRequest)(nil),  // 6: protobuf.StreamingMaximumRequest
	(*StreamingMaximumResponse)(nil), // 7: protobuf.StreamingMaximumResponse
}
var file_vend_protobuf_calculator_proto_depIdxs = []int32{
	0, // 0: protobuf.CalculatorService.Sum:input_type -> protobuf.SumRequest
	2, // 1: protobuf.CalculatorService.StreamingSum:input_type -> protobuf.StreamingSumRequest
	4, // 2: protobuf.CalculatorService.StreamingAverage:input_type -> protobuf.StreamingAverageRequest
	6, // 3: protobuf.CalculatorService.StreamingMaximum:input_type -> protobuf.StreamingMaximumRequest
	1, // 4: protobuf.CalculatorService.Sum:output_type -> protobuf.SumResponse
	3, // 5: protobuf.CalculatorService.StreamingSum:output_type -> protobuf.StreamingSumResponse
	5, // 6: protobuf.CalculatorService.StreamingAverage:output_type -> protobuf.StreamingAverageResponse
	7, // 7: protobuf.CalculatorService.StreamingMaximum:output_type -> protobuf.StreamingMaximumResponse
	4, // [4:8] is the sub-list for method output_type
	0, // [0:4] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_vend_protobuf_calculator_proto_init() }
func file_vend_protobuf_calculator_proto_init() {
	if File_vend_protobuf_calculator_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_vend_protobuf_calculator_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SumRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vend_protobuf_calculator_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SumResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vend_protobuf_calculator_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StreamingSumRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vend_protobuf_calculator_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StreamingSumResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vend_protobuf_calculator_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StreamingAverageRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vend_protobuf_calculator_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StreamingAverageResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vend_protobuf_calculator_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StreamingMaximumRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_vend_protobuf_calculator_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*StreamingMaximumResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_vend_protobuf_calculator_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   8,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_vend_protobuf_calculator_proto_goTypes,
		DependencyIndexes: file_vend_protobuf_calculator_proto_depIdxs,
		MessageInfos:      file_vend_protobuf_calculator_proto_msgTypes,
	}.Build()
	File_vend_protobuf_calculator_proto = out.File
	file_vend_protobuf_calculator_proto_rawDesc = nil
	file_vend_protobuf_calculator_proto_goTypes = nil
	file_vend_protobuf_calculator_proto_depIdxs = nil
}
