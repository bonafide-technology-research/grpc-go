# grpc-go

Reference project for gRPC framework in GoLang. 

## Protocol Buffers

`grpc-generate.sh` is a script which executes protocol buffers code gen for the `.proto` file [here](./internal/calculator/protobuf/calculator.proto).

## Examples

### Server

gRPC Server application

View [server process here](./cmd/server/main.go).

### Unary API Client

gRPC Client application which consume the Unary API

View [unary client process here](./cmd/unary/main.go).

### Server Streaming API Client

gRPC Client application which consume the Server-Streaming API

View [server-streaming client process here](./cmd/server_streaming/main.go).

### Client Streaming API Client

gRPC Client application which consume the Client-Streaming API

View [client-streaming client process here](./cmd/client_streaming/main.go).

### Bi-Directional API Client

gRPC Client application which consume the Bi-Directional API

View [server-streaming client process here](./cmd/bi_directional_streaming/main.go).

## CI/CD

`grpc-go` leverages GitLab CI to where the pipelines are defined in [.gitlab-ci.yml](./.gitlab-ci.yml).
