module gitlab.com/bonafide-technology-research/grpc-go

go 1.14

require (
	github.com/brianvoe/gofakeit/v5 v5.11.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.4.3
	github.com/oklog/ulid/v2 v2.0.2
	github.com/stretchr/testify v1.6.1
	google.golang.org/grpc v1.33.2
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.0.1 // indirect
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
