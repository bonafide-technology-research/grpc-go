package main

import (
	"context"
	"fmt"
	"log"

	"google.golang.org/grpc"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/request"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

func main() {
	fmt.Println("Executing gRPC Unary API client...")

	// create a client connection with the remote gRPC server.
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf(err.Error())
	}

	// instantiate Calculator Service client.
	c := protobuf.NewCalculatorServiceClient(cc)

	// invoke protobuf.Sum unary API.
	fmt.Println("Invoking CalculatorService.Sum with args:", 1, ",", 2)
	res, err := c.Sum(context.Background(), request.NewSumRequest(1, 2).Request)
	if err != nil {
		log.Println(err.Error())
	} else {
		fmt.Println("Result:", res.Result)
	}

	// invoke protobuf.Sum unary API with invalid arguments
	fmt.Println("\nInvoking CalculatorService.Sum with args:", -1, ",", 1)
	res, err = c.Sum(context.Background(), request.NewSumRequest(-1, 1).Request)
	if err != nil {
		log.Println(err.Error())
	} else {
		fmt.Println("Result:", res.Result)
	}
}
