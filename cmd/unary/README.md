# Unary Streaming API

The Unary Streaming API implements a gRPC endpoint which determines the sum for the given set of numbers.

## Scenario

![](../../res/diagrams-unary.png)

## Running the example

Start the gRPC Server

```
go run ../server/main.go
```

Start the Client

```
go run ./main.go
```