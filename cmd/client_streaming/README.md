# Client-Streaming API

The Client Streaming API implements a gRPC endpoint which determines the average of the streaming numbers from the client.

## Scenario

![](../../res/diagrams-client-streaming.png)

## Running the example

Start the gRPC Server

```
go run ../server/main.go
```

Start the Client

```
go run ./main.go
```