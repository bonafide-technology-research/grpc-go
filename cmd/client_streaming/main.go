package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/oklog/ulid/v2"
	"google.golang.org/grpc"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/request"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

func main() {
	fmt.Println("Executing gRPC Client-Streaming API client...")

	// create a client connection with the remote gRPC server.
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf(err.Error())
	}

	// instantiate Calculator Service client.
	c := protobuf.NewCalculatorServiceClient(cc)

	// create ulid used to distinguish streaming-clients from others.
	t := time.Unix(1000000, 0)
	entropy := ulid.Monotonic(rand.New(rand.NewSource(t.UnixNano())), 0)
	uid := ulid.MustNew(ulid.Timestamp(t), entropy).String()

	stream, err := c.StreamingAverage(context.Background())
	if err != nil {
		log.Fatalf(err.Error())
	}

	// stream 10 random numbers to server; these numbers are to be averaged.
	for i := 0; i < 10; i++ {
		n := rand.Int63n(100)
		fmt.Println("Sending: ", n)
		err := stream.Send(request.NewStreamingAverageRequest(uid, n).Request)
		if err != nil {
			log.Fatalf(err.Error())
		}
		time.Sleep(2 * time.Second)
	}

	r, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf(err.Error())
	}
	fmt.Printf("Average: %f\n ", r.GetResult())
}
