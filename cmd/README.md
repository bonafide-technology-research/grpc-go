# cmd

Directory which contains all of the entry-points for each executable.

## bi_directional_streaming

Directory containing the Calculator Bi-Directional API executable entry-point.

## client_streaming

Directory containing the Calculator Client-Streaming API executable entry-point.

## server

Directory containing the Calculator Service server executable entry-point.

## server_streaming

Directory containing the Calculator Server-Streaming API executable entry-point.

## unary

Directory containing the Calculator Service unary API executable entry-point.