# Bi-Directional Streaming API

The Bi-Directional Streaming API implements a gRPC endpoint which determines the maximum value for the given set of numbers.

## Scenario

![](../../res/diagrams-bi-directional-streaming.png)

## Running the example

Start the gRPC Server

```
go run ../server/main.go
```

Start the Client

```
go run ./main.go
```