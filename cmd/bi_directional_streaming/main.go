package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"math/rand"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/request"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

func main() {
	fmt.Println("Executing gRPC Bi-Directional-Streaming API client...")

	// create a client connection with the remote gRPC server.
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf(err.Error())
	}

	// instantiate Calculator Service client.
	c := protobuf.NewCalculatorServiceClient(cc)

	stream, err := c.StreamingMaximum(context.Background())
	if err != nil {
		log.Fatalf(err.Error())
	}

	// waitGroup defined to allow both go-routines to complete before halting the program.
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			n := rand.Int63n(100)
			fmt.Println("Sending: ", n)
			err := stream.Send(request.NewStreamingMaxRequest(n).Request)
			if err != nil {
				log.Fatalf(status.Error(codes.Internal, err.Error()).Error())
			}
			time.Sleep(2 * time.Second)
		}
		err := stream.CloseSend()
		if err != nil {
			log.Fatalf(status.Error(codes.Internal, err.Error()).Error())
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				fmt.Println("ending bi-directional stream from client")
				break
			}
			if err != nil {
				log.Fatalf(status.Error(codes.Internal, err.Error()).Error())
			}
			fmt.Printf("Maximum number is %d\n", res.GetResult())
		}
		err := stream.CloseSend()
		if err != nil {
			log.Fatalf(status.Error(codes.Internal, err.Error()).Error())
		}
	}()

	wg.Wait()
}
