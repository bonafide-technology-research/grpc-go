# gRPC API Server

gRPC Server implementation which registers the Calculator Service.


## Running the server

Start the gRPC Server

```
go run ../server/main.go
```