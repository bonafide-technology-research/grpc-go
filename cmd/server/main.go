package main

import (
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/server"
	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/server/store"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

func main() {
	fmt.Println("Starting gRPC server...")

	// listen for gRPC client connections.
	listener, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf(err.Error())
	}

	// instantiate a gRPC server.
	s := grpc.NewServer()

	// registry Calculator Service server implementation.
	protobuf.RegisterCalculatorServiceServer(s, server.New(store.NewInMemAverageStore()))

	// listen for connections.
	if s.Serve(listener); err != nil {
		log.Fatalf(err.Error())
	}
}
