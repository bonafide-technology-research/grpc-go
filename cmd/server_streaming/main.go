package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	"google.golang.org/grpc"

	"gitlab.com/bonafide-technology-research/grpc-go/internal/calculator/domain/request"
	"gitlab.com/bonafide-technology-research/grpc-go/vend/protobuf"
)

func main() {
	fmt.Println("Executing gRPC Server-Streaming API client...")

	// create a client connection with the remote gRPC server.
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf(err.Error())
	}

	// instantiate Calculator Service client.
	c := protobuf.NewCalculatorServiceClient(cc)
	ticker := time.NewTicker(5 * time.Second)

	// waitGroup defined to allow both go-routines to complete before halting the program.
	var wg sync.WaitGroup
	wg.Add(1)

	// Within a go-routine, every tick, invoke a new StreamingSum request to continuously count the streaming sum.
	go func() {
		defer wg.Done()
		for {
			select {
			case _ = <-ticker.C:
				stream, err := c.StreamingSum(
					context.Background(),
					request.NewStreamingSumRequest(rand.Int63n(100)).Request,
				)
				if err != nil {
					log.Fatalf(err.Error())
				}

				res, err := stream.Recv()
				if err != nil {
					log.Fatalf(err.Error())
				}

				fmt.Printf("Current Total: %d\n\n", res.Result)
			}
		}
	}()

	wg.Wait()
}
